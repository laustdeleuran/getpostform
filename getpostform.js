/*************************************************
**  jQuery getPostForm version 1.0
**  Copyright Laust Deleuran, licensed GNU GPL v3
**  http://dev.ljd.dk/getpostform
**************************************************/
(function( $ ){
    $.fn.getPostForm = function(options) { // Do you get the double meaning - do you, soldier, do you?!
        // Settings
        var settings = {
            postAttribute: 'data-method',
            className: 'postGetForm',
            createFormId: 'postGetFormAutoPoster',
            doRemoveOldFields: true,
            affectedFields: 'input[name],select,textarea'
        };
        if (options) { $.extend(settings, options); }
        
        // Functions
        var gpForm = {
            create: function(container, className, elName, value){
                return container.append('<input type="hidden" class="' + className + '" name="' + elName + '" value="' + value + '" />');
            },
            handle: function(method, container, className, elName, value) {
                if (method.toLowerCase() == "post") {
                    return gpForm.create(container, className, elName, value);
                } else {
                    return elName + "=" + value;
                }
            }
        }
        
        // Loop and return object collection
        return this.each(function(){
            if ($(this).is('form')) {
                $(this).submit(function(event){
                    event.preventDefault();
                    var actionUrl = $(this).attr('action');
                    var smartActionUrl = "";
                    if (actionUrl.indexOf("?") == -1) {
                        actionUrl += "?";
                    }
                    $('#postGetFormAutoPoster').remove();
                    $('<form id="postGetFormAutoPoster"></form>').appendTo('body');
                    var $form = $('#postGetFormAutoPoster');
                    $form.attr('method','post');
                    $container = $(this);
                    $container.find(settings.affectedFields).each(function(){
                        var $this = $(this);
                        var curPostAttr = $this.attr(settings.postAttribute) ? $this.attr(settings.postAttribute) : "get" ;
                        var handler; 
                        
                        if ($this.is('select[multiple]')) {
                            var multiple = $this.val() || [];
                            $.each(multiple,function(index,value) {
                                handler = gpForm.handle(curPostAttr, $form, settings.className, $this.attr('name'), value);
                                if (typeof(handler) == "string") {
                                    if (smartActionUrl.length > 0) {
                                        smartActionUrl += "&";
                                    }
                                    smartActionUrl += handler;
                                }
                            });
                        } else if ($this.is('input[type="radio"]')) {
                            if ($this.is(':checked')) {
                                handler = gpForm.handle(curPostAttr, $form, settings.className, $this.attr('name'), $this.val());
                                if (typeof(handler) == "string") {
                                    if (smartActionUrl.length > 0) {
                                        smartActionUrl += "&";
                                    }
                                    smartActionUrl += handler;
                                }
                            }
                        } else {
                            handler = gpForm.handle(curPostAttr, $form, settings.className, $this.attr('name'), $this.val());
                            if (typeof(handler) == "string") {
                                if (smartActionUrl.length > 0) {
                                    smartActionUrl += "&";
                                }
                                smartActionUrl += handler;
                            }
                        }
                    });
                    smartActionUrl = actionUrl + smartActionUrl;
                    $form.attr('action', smartActionUrl);
                    if (settings.doRemoveOldFields) {
                        $form.find(settings.affectedFields).not('.'+settings.className).remove();
                    }
                    $form.submit();
                });
            }
        });/**/
    };
})( jQuery );